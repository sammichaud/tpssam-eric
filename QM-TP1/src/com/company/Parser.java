package com.company;
//Hypothese : Nous comptons une ligne de commentaire vide (ligne vide entre /* */) comme une ligne en soi
//Notre implementation réduit le nombre de lignes de commentaires de la javadoc si possible
import com.github.javaparser.StaticJavaParser;
import com.github.javaparser.ast.CompilationUnit;
import com.github.javaparser.ast.body.ClassOrInterfaceDeclaration;
import com.github.javaparser.ast.body.MethodDeclaration;
import com.github.javaparser.ast.comments.Comment;
import com.github.javaparser.ast.visitor.VoidVisitor;
import com.github.javaparser.ast.visitor.VoidVisitorAdapter;

import java.io.*;
import java.util.List;
import java.util.stream.Collectors;

public class Parser {
    private static final String FILE_PATH = "BestClass.java";
    static int cLOC = 0;
    static int mLOC = 0;
    static int cCLOC = 0;
    static int mCLOC = 0;
    static int cDC = 0;
    static int mDC = 0;

    Parser() {
        CompilationUnit cu = null;
        try {
            cu = StaticJavaParser.parse(new File(FILE_PATH));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        VoidVisitor<?> methodNameVisitor = new MethodPrinter();
        methodNameVisitor.visit(cu, null);
        VoidVisitor<?> classNameVisitor = new ClassPrinter();
        classNameVisitor.visit(cu, null);
    }

    static class MethodPrinter extends VoidVisitorAdapter<Void> {
        @Override
        public void visit(MethodDeclaration md, Void arg) {
            mCLOC = 0;
            mLOC = 0;
            List<Comment> cc = md.getAllContainedComments();
            cc.forEach(c -> mCLOC += c.getRange().get().getLineCount() );
            if (md.getComment().isPresent())
                mCLOC += md.getComment().get().getRange().get().getLineCount();

            Reader r = new StringReader(md.getBody().toString());
            BufferedReader br = new BufferedReader(r);
            String line;
            try {
                int count = 0;
                while ((line = br.readLine()) != null) {
                    if (!"".equals(line.trim())){
                        count++;
                    }
                }
                mLOC = count;
            } catch (IOException e) {
                e.printStackTrace();
            }
            System.out.println("mCLOC(" + md.getName() + ") : " + mCLOC);
            System.out.println("mLOC(" + md.getName() + ") : " + mLOC);
            System.out.println("mDC(" + md.getName() + ") : " + ((double)mCLOC/(double)mLOC) +"\n");
            super.visit(md, arg);
        }
    }
    static class ClassPrinter extends VoidVisitorAdapter<Void> {
        @Override
        public void visit(ClassOrInterfaceDeclaration cd, Void arg) {
            cCLOC = 0;
            cLOC = 0;
            List<Comment> comments = cd.getAllContainedComments().stream().collect(Collectors.toList());
            comments.forEach(c -> cCLOC += c.getRange().get().getLineCount());
            if (cd.getComment().isPresent())
                cCLOC += cd.getComment().get().getRange().get().getLineCount();
            Reader r = new StringReader(cd.asClassOrInterfaceDeclaration().toString());
            BufferedReader br = new BufferedReader(r);
            String line;
            try {
                int count = 0;
                while ((line = br.readLine()) != null) {
                    if (!"".equals(line.trim())){
                        count++;
                    }
                }
                cLOC = count;
            } catch (IOException e) {
                e.printStackTrace();
            }
            System.out.println("cCLOC(" + cd.getName() +") : " + cCLOC);
            System.out.println("cLOC(" + cd.getName() + ") : " + cLOC);
            System.out.println("cDC(" + cd.getName() + ") : " + (double)cCLOC/(double)cLOC +"\n");
            super.visit(cd, arg);
        }
    }
}

